﻿
var firebaseConfig = {
    apiKey: "AIzaSyBT1YOXJEM6rMrmQYVBegtJQ9mc9frMhhw",
    authDomain: "puckie-59c9f.firebaseapp.com",
    databaseURL: "https://puckie-59c9f.firebaseio.com",
    projectId: "puckie-59c9f",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


// Get a reference to the database service
var database_ref = firebase.database();

var userId; 
var correo_usuario_actual;
var rango_usuario_actual

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    userId = firebase.auth().currentUser.uid; // https://firebase.google.com/docs/reference/js/firebase.auth.Auth?authuser=0#currentuser
                                              // https://firebase.google.com/docs/reference/js/firebase.User?authuser=0#uid
    console.log("UserID: " + userId);
    
    correo_usuario_actual = firebase.auth().currentUser.email;
    correo_usuario_actual = correo_usuario_actual.replace(/\./g,' ');
    firebase.database().ref('Usuarios/' + correo_usuario_actual + '/rango').once("value").then(function(rank){
      rango_usuario_actual = rank.val(); 
    });

    child_added(); // Llamo a este método una vez que efectivamente tendo el userID. Si no lo hago así se rompe la ruta. emoroni.
  } else {
    // No user is signed in.
    console.log("No hay un usuario.");
    //alert('No has iniciado sesión. Por favor inicia sesión o registrate para poder ingresar a esta pagina');
    window.location.replace("./index.html")
  }
});

function entrada_mp (){

  var cantidad_en_proceso = document.getElementById("cantidad_en_proceso").value;
 
  if(rango_usuario_actual != "Externo") { 
    if(cantidad_en_proceso != '')
    {
      firebase.database().ref('Productos/Pucket/cantidad_en_proceso').transaction(function(cantidad_proceso_actual){
        cantidad_proceso_actual = Number(cantidad_proceso_actual) + Number(cantidad_en_proceso);
        return cantidad_proceso_actual;
      });
      firebase.database().ref('Productos/Dominó/cantidad_en_proceso').transaction(function(cantidad_proceso_actual){
        cantidad_proceso_actual = Number(cantidad_proceso_actual) + Number(cantidad_en_proceso);
        return cantidad_proceso_actual;
      });
      firebase.database().ref('Productos/Tateti/cantidad_en_proceso').transaction(function(cantidad_proceso_actual){
        cantidad_proceso_actual = Number(cantidad_proceso_actual) + Number(cantidad_en_proceso);
        return cantidad_proceso_actual;
      });
      location.reload();
  
      document.getElementById("cantidad_en_proceso").value = '';
      alert('Carga exitosa');  
    }
    else if(cantidad_en_proceso == '')
    {
      alert('Complete todos los campos por favor.');
    }
  } else {
    alert('Para ingresar materia prima debes ser parte de la compañia');
  }
}

function escribir_db (){
 
var producto = document.getElementById("producto").value;
var cantidad_disponible = document.getElementById("cantidad_disponible").value;

if(rango_usuario_actual != "Externo") { 
  if(producto != '' && cantidad_disponible != '')
  { 
      firebase.database().ref('Productos/' + producto + '/cantidad_disponible').transaction(function(cantidad_actual){
        cantidad_actual = Number(cantidad_actual) + Number(cantidad_disponible);
        return cantidad_actual;
      });
      firebase.database().ref('Productos/' + producto + '/cantidad_en_proceso').transaction(function(cantidad_actual){
        cantidad_actual = Number(cantidad_actual) - Number(cantidad_disponible);
        return cantidad_actual;
      });
      location.reload();
  
      document.getElementById("producto").value = '';
      document.getElementById("cantidad_disponible").value = '';
      alert('Carga exitosa');  

  } else if(producto == '' || cantidad_disponible == '')
  {
    alert('Complete todos los campos por favor.');
  }
} else {
  alert('Para ingresar stock debes ser parte de la compañia');
}
}

/************************************************* */
/* Funcion ventas */
/************************************************* */
function ventas1_db (){
 
  var producto = document.getElementById("producto").value;
  var cantidad_vendida = document.getElementById("cantidad_vendida").value;
  var cliente = document.getElementById("cliente").value;
  var vendedor = document.getElementById("Vendedor").value; 

  if(rango_usuario_actual != "Externo") {
    if(producto != '' && cantidad_vendida != '' && cliente != '' && vendedor != '')
    { 
          firebase.database().ref('Ventas/').push({
            ie: "datos",
            producto: producto,
            cantidad_vendida : cantidad_vendida,
            cliente : cliente,
            vendedor : vendedor,
        
        });
        
        location.reload();
        
        /*
        document.getElementById("producto").value = '';
        document.getElementById("cantidad_vendida").value = '';
        document.getElementById("cliente").value = '';
        document.getElementById("Vendedor").value = '';
        
        alert('Carga exitosa');  
        */

    } else if(producto == '' || cantidad_vendida == '' || vendedor == '' || cliente == '')
    {
      alert('Complete todos los campos por favor.');
    }
  } else {
    alert('Para ingresar ventas debes ser parte de la compañia');
  }  
  }

function ventas2_db (){
 
  var producto = document.getElementById("producto").value;
  var cantidad_vendida = document.getElementById("cantidad_vendida").value; 

    if(producto != '' && cantidad_vendida != '')
    { 
        firebase.database().ref('Productos/' + producto + '/cantidad_disponible').transaction(function(cantidad_actual){
          cantidad_actual = Number(cantidad_actual) - Number(cantidad_vendida);
          return cantidad_actual;
    }); 

} else if(producto == '' || cantidad_vendida == '')
{
  alert('Complete todos los campos por favor.');
}
}
  
function ventas_db(){
  ventas1_db();
  ventas2_db();
  alert('Carga exitosa'); 
}

/************************************************* */
/* Fin funcion ventas */
/************************************************* */

function usuarios_db (){

  var tipo_usuario = document.getElementById("tipo_usuarios").value;
  var correo_electronico = document.getElementById("correo_electronico").value;
  var correo_electronico_sinP;
  
  if(rango_usuario_actual == "Administrador") {
    if(tipo_usuario != '' && correo_electronico != '')
    { 
      correo_electronico_sinP = correo_electronico.replace(/\./g,' ');
      firebase.database().ref('Usuarios/' + correo_electronico_sinP + '/rango').transaction(function(usuario){
        usuario = tipo_usuario;
        return usuario;
      });

      firebase.database().ref('Usuarios/' + correo_electronico_sinP + '/email').transaction(function(email){
        email = correo_electronico;
        return email;
      });
      
      // document.getElementById("tipo_usuario").value = '';    
      document.getElementById("correo_electronico").value = '';    
      alert('Carga exitosa');          
      
      location.reload();
    }   
    else if(tipo_usuario == '' || correo_electronico == '')
    {
      alert('Complete todos los campos por favor.');
    }
  } else if (rango_usuario_actual == 'Privilegios' && (tipo_usuario == 'Externo' || tipo_usuario == 'Empleado')){
    if(tipo_usuario != '' && correo_electronico != '')
    { 
      correo_electronico_sinP = correo_electronico.replace(/\./g,' ');
      firebase.database().ref('Usuarios/' + correo_electronico_sinP + '/rango').transaction(function(usuario){
        usuario = tipo_usuario;
        return usuario;
      });

      firebase.database().ref('Usuarios/' + correo_electronico_sinP + '/email').transaction(function(email){
        email = correo_electronico;
        return email;
      });
      
      // document.getElementById("tipo_usuario").value = '';    
      document.getElementById("correo_electronico").value = '';    
      alert('Carga exitosa');          
      
      location.reload();
    }   
    else if(tipo_usuario == '' || correo_electronico == '')
    {
      alert('Complete todos los campos por favor.');
    }
  } else {
    alert('Para cambiar los rangos de los usuarios debes ser administrador o tener privilegios');
  }  
}

function child_added (){

  
  firebase.database().ref('Productos/').on('child_added', function(data) {
    
    var del_id = data.key + "_del";
    var upd_id = data.key + "_upd";

    $(tabla_datos).append('<div class="card text-white bg-dark mb-3 col-sm-4" style="max-width: 20rem;"><div class="card-header">' + data.val().ie + '</div><div class="card-body"><h5 class="card-title">Cantidad disponible: ' + data.val().cantidad_disponible + '</h5><h5 class="card-title">Cantidad en proceso: ' + data.val().cantidad_en_proceso + '</h5></div></div>');
    // $(tabla_datos).append( '<tr><td>' + data.val().ie + '</td><td>' + data.val().cantidad_disponible  + '</td><td>' + data.val().cantidad_en_proceso + '</td></tr>'); 
  });
  
  
  firebase.database().ref('Ventas/').on('child_added', function(data) {
    
    var del_id = data.key + "_del";
    var upd_id = data.key + "_upd";

    $(tabla_datos_ventas).append( '<tr><td>' + data.val().vendedor  + '</td><td>' + data.val().cliente + '</td><td>' + data.val().cantidad_vendida + '</td><td>' + data.val().producto + '</td></tr>'); 
  });
  

  
  firebase.database().ref('Usuarios/').on('child_added', function(data) {
    
    var del_id = data.key + "_del";
    var upd_id = data.key + "_upd";
    

    console.log(data.val().email);
    console.log(data.val().rango);
    console.log(data.key);
    console.log(del_id);

   // $(columna_coleccion).append('<div class="col-sm-2 elementos_listado">' + data.val().producto + '</div>');
    $(columna_coleccion_user).append('<div class="col-sm-6 elementos_listado" style="margin-left: 0; color: #ffffff; font-size: 20px;">' + data.val().email + '</div>'); 
    $(columna_coleccion_user).append('<div class="col-sm-6 elementos_listado" style="margin-left: 0; color: #ffffff; font-size: 20px;">' + data.val().rango + '</div>'); 
    //$(columna_coleccion).append(indexar_botones(data.key));
 
    /*
    document.getElementById(del_id).addEventListener('click', function () {delete_db('Productos/'+ '/' + data.key)}, false);
    document.getElementById(upd_id).addEventListener('click', function () {update_db('Productos/'+ '/' + data.key)}, false);
    */

  });
}
/*
function child_added (){
  firebase.database().ref('Usuarios/').on('child_added', function(data) {
    
    var del_id = data.key + "_del";
    var upd_id = data.key + "_upd";
    

    console.log(data.val().email);
    console.log(data.val().rango);
    console.log(data.key);
    console.log(del_id);

   // $(columna_coleccion).append('<div class="col-sm-2 elementos_listado">' + data.val().producto + '</div>');
    $(columna_coleccion_user).append('<div class="col-sm-6 elementos_listado" style="margin-left: 0; !important">' + data.val().email + '</div>'); 
    $(columna_coleccion_user).append('<div class="col-sm-6 elementos_listado" style="margin-left: 0; !important">' + data.val().rango + '</div>'); 
    //$(columna_coleccion).append(indexar_botones(data.key));
 
    
    // document.getElementById(del_id).addEventListener('click', function () {delete_db('Productos/'+ '/' + data.key)}, false);
    // document.getElementById(upd_id).addEventListener('click', function () {update_db('Productos/'+ '/' + data.key)}, false);
    

  });
}*/
 
/*
function indexar_botones (key){
  var boton_borrar = '<button type="button" class="btn btn-danger boton-borrar" id="' + key + '_del"> Borrar </button>';
  var boton_update = '<button type="button" class="btn btn-info boton-update" id="' + key + '_upd">Editar</button>';
  return boton_borrar + boton_update;
}
 
function delete_db (path){
  // console.log("Bandera Delete Element"); // Descomentar para debugging. emoroni.
  firebase.database().ref(path).remove();
  alert('Elemento eliminado.');
  location.reload();
}
 
function update_db (path){
  var txt;
  var producto = prompt("Ingrese el nombre: ", );
  var cantidad_disponible = prompt("Ingrese el producto: ",);
  var cantidad_en_proceso = prompt("Ingrese la cantidad: ",);
  
  var update_data = {
    ie: "datos",
    producto: producto,
    cantidad_disponible : cantidad_disponible,
    cantidad_en_proceso : cantidad_en_proceso,
  }
 
  firebase.database().ref(path).update(update_data);
  location.reload();  
}
*/