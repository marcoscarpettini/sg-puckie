'use strict';

//Version de archivo cache 
const CACHE_NAME = 'static-cache-4';

//Lista de archivos para guardar en cache.
const FILES_TO_CACHE = [
	'offline.html',
	'manifest.json',
    'ingresado/images/Puckie/zorro confundido.gif',
	'ingresado/images/Puckie/logo-color.png',
	'ingresado/index2.html',
	'ingresado/images/Puckie/Zorro blanco.png',
	'ingresado/images/Imagenes_nuevas_04_08/Tateti.jpg',
	'ingresado/images/Imagenes_nuevas_04_08/Los_3_juntos.jpg',
	'ingresado/images/Imagenes_nuevas_04_08/Pucket_acercamiento.jpg',
	'ingresado/images/Imagenes_nuevas_04_08/Fichas.jpg',
	'ingresado/images/Imagenes_nuevas_04_08/Pucket_con_fichas.jpg',
	'ingresado/images/Puckie/Yara Guazu.jpg',
	'ingresado/images/Puckie/Municipalidad_de_Campana.jpg', 
	'index.html',
	'ingresado/images/Iconos/Puckie/Logo-tamanos/logo-color-152x152.png',
	'database.html',
	'database-venta.html',
	'database-usuarios.html',
	'realtime.js',
	'auth.js',
	'ingresado/style.css',
	'ingresado/index.css',
	'bootstrap.min.css',
];

/*
 A continuación, debemos actualizar el evento install para indicar al service worker que precachee 
 la página sin conexión:
*/

self.addEventListener('install', (evt) => {
	console.log('[ServiceWorker] Install');
	// CODELAB: Precache static resources here.
	evt.waitUntil(
		caches.open(CACHE_NAME).then((cache) => {
			console.log('[ServiceWorker] Pre-caching offline page');
			return cache.addAll(FILES_TO_CACHE);
		})
	);

	self.skipWaiting();
});

// Limpieza de páginas sin conexión antiguas

self.addEventListener('activate', (evt) => {
	console.log('[ServiceWorker] Activate');
	// CODELAB: Remove previous cached data from disk.
	evt.waitUntil(
		caches.keys().then((keyList) => {
			return Promise.all(keyList.map((key) => {
				if (key !== CACHE_NAME) {
					console.log('[ServiceWorker] Removing old cache', key);
					return caches.delete(key);
				}
			}));
		})
	);

	self.clients.claim();
});

// Solicitudes de red fallidas

self.addEventListener('fetch', (evt) => {
	console.log('[ServiceWorker] Fetch', evt.request.url);
	// CODELAB: Add fetch event handler here.
	if (evt.request.mode !== 'navigate') {
		// Not a page navigation, bail.
		return;
	}

//Opción: Primero red y luego cache.
	evt.respondWith(
		fetch(evt.request)
			.catch(() => {
				return caches.open(CACHE_NAME)
					.then((cache) => {
						cache.match('ingresado/images/Puckie/zorro confundido.gif');
						cache.match('ingresado/images/Puckie/logo-color.png');
						return cache.match('offline.html');
					});
			})
	);
});