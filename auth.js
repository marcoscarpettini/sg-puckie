﻿
var firebaseConfig = {
   apiKey: "AIzaSyBT1YOXJEM6rMrmQYVBegtJQ9mc9frMhhw",
    authDomain: "puckie-59c9f.firebaseapp.com",
    databaseURL: "https://puckie-59c9f.firebaseio.com",
    projectId: "puckie-59c9f",
};

   firebase.initializeApp(firebaseConfig);
   const auth = firebase.auth(); 
   console.log("Hola inicializar...");

var btn = document.getElementById("boton_ingresar");

// Get a reference to the database service
var database_ref = firebase.database();


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function registrarse(){
var email = document.getElementById("usuario_registro").value;
var password = document.getElementById("contraseña_registro").value;
var email_sinP;

firebase.auth().createUserWithEmailAndPassword(email, password)
.then(function() {
   verificar();
   email_sinP = email.replace(/\./g,' ');
   firebase.database().ref('Usuarios/' + email_sinP).set({
      ie: "Rango",
      rango: "Externo",
      email: email,
   });
})
.catch(function(error) {
   // Handle Errors here.
   var errorCode = error.code;
   var errorMessage = error.message;
   console.log(error.code);
   console.log(error.message);

   if (errorCode === 'auth/invalid-email') {
         alert('La dirección de correo es inválida.');
      }
   if (errorCode === 'auth/email-already-in-use') {
         alert('Ya existe una cuenta con esta dirección de correo.');
   }
   if (errorCode === 'auth/weak-password') {
         alert('La contraseña es débil.');
      }
   if (errorCode === 'auth/operation-not-allowed') {
   alert('Operación no permitida.');
   }
   // ...
 });

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function chequear_user() {

   var email = document.getElementById("usuario").value;
   var password = document.getElementById("contraseña").value;
   console.log(email);
   console.log(password);

   firebase.auth().signInWithEmailAndPassword(email, password)
      .then(function () {  //Caso efectivo
         if (firebase.auth().currentUser.emailVerified){
            console.log("Bandera arriba, todo salió bien");
            window.location.replace("./ingresado/index2.html");
         } else {
            alert('Verifica tu dirección de correo electrónico antes de iniciar sesión.');
            firebase.auth().signOut().then(function() {
               // Sign-out successful.
               window.location.replace("./index.html");
            }).catch(function(error) {
               // An error happened.
               alert("Ha ocurrido un error, intente nuevamente.");
            });
         }
      })
      .catch(function (error) { //Caso rechazado, evaluo el error
         // console.log("Bandera error, algo no salió bien"); // Descomentar para verificar el funcionamiento. emoroni.
         var errorCode = error.code;
         if (errorCode === 'auth/invalid-email') {
            alert('La dirección de correo es inválida.');
         }
         if (errorCode === 'auth/user-disabled') {
            alert('El usuario ingresado está deshabilitado.');
         }
         if (errorCode === 'auth/user-not-found') {
            alert('El usuario ingresado no se encuentra registrado.');
         }
         if (errorCode === 'auth/wrong-password') {
            alert('La contraseña no es correcta.');
         }
      });
   }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   function verificar() {
      var user = firebase.auth().currentUser;
      user.sendEmailVerification().then(function(){
         console.log('enviando correo...');
         alert('El registro se ha completado con exito, para continuar verifique su dirección de correo electrónico.');
      })
      .catch(function(error){
         console.log(error);
      });
   }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function cerrar_sesion() {
   firebase.auth().signOut().then(function() {
      // Sign-out successful.
      window.location.replace("./index.html");
    }).catch(function(error) {
      // An error happened.
      alert("Ha ocurrido un error, intente nuevamente.");
    });
}

function cerrar_sesion2() {
   firebase.auth().signOut().then(function() {
      // Sign-out successful.
      window.location.replace("../index.html");
    }).catch(function(error) {
      // An error happened.
      alert("Ha ocurrido un error, intente nuevamente.");
    });
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

firebase.auth().onAuthStateChanged(function(user) {
   if(user){

   }
   else {
   }
});

